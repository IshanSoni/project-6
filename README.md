NOTE TO THE GRADER: Group members of my team were having issues forking and pulling the repository
                    so they sent whatever work they had done to me through social media and I 
                    tried my best to specify the contribution from each member through comments.     
                    Reason we have only three commits which are from me is because we set up the 
                    repository at last minute and were not able to fix the issues on time. We 
                    have learned from our mistake and will not repeat this behavior again. It will be very kind of you to forgive us newbies this time.                        
                    Nonetheless, it is up to you, the grader, to decide whether this group 
                    deserves the credit for this project or not. Thank you.

-Ishan Soni