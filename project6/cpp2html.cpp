/*
 * CSc103 Project 5: Syntax highlighting, part two.
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: ~10
 */

#include "fsm.h"
using namespace cppfsm;
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <set>
using std::set;
#include <map>
using std::map;
#include <initializer_list> // for setting up maps without constructors.

// enumeration for our highlighting tags:
enum {
	hlstatement,  // used for "if,else,for,while" etc...
	hlcomment,    // for comments
	hlstrlit,     // for string literals
	hlpreproc,    // for preprocessor directives (e.g., #include)
	hltype,       // for datatypes and similar (e.g. int, char, double)
	hlnumeric,    // for numeric literals (e.g. 1234)
	hlescseq,     // for escape sequences
	hlerror,      // for parse errors, like a bad numeric or invalid escape
	hlident       // for other identifiers.  Probably won't use this.
};

// usually global variables are a bad thing, but for simplicity,
// we'll make an exception here.
// initialize our map with the keywords from our list:
map<string, short> hlmap = {
#include "res/keywords.txt"
};
// note: the above is not a very standard use of #include...

// map of highlighting spans:
map<int, string> hlspans = {
	{hlstatement, "<span class='statement'>"},
	{hlcomment, "<span class='comment'>"},
	{hlstrlit, "<span class='strlit'>"},
	{hlpreproc, "<span class='preproc'>"},
	{hltype, "<span class='type'>"},
	{hlnumeric, "<span class='numeric'>"},
	{hlescseq, "<span class='escseq'>"},
	{hlerror, "<span class='error'>"}
};
// note: initializing maps as above requires the -std=c++0x compiler flag,
// as well as #include<initializer_list>.  Very convenient though.
// to save some typing, store a variable for the end of these tags:
string spanend = "</span>";

string translateHTMLReserved(char c) {
	switch (c) {
		case '"':
			return "&quot;";
		case '\'':
			return "&apos;";
		case '&':
			return "&amp;";
		case '<':
			return "&lt;";
		case '>':
			return "&gt;";
		case '\t': // make tabs 4 spaces instead.
			return "&nbsp;&nbsp;&nbsp;&nbsp;";
		default:
			char s[2] = {c,0};
			return s;
	}
}



  
string processOneLine(string s)
{              
   string newS = "";                            //I did from start of the function
   int state = start;                           //until case scanid 
   for(int i = 0; i < s.length(); i++)           
   {
      string temp = "";
      updateState(state, s[i]);

	  switch(state){
		
	     case scanid:
		    while(state == scanid){
		    temp += s[i] ;
			i++;
			updateState(state,s[i]);
		    }
                        
			if (state!= 1){
			map<string, short>::iterator it;
			it = hlmap.find(temp);
			if (it != hlmap.end()) {
			   newS += hlspans[hlmap[temp]] + temp + spanend + translateHTMLReserved(s[i]);
			   }
			else {
			   newS += temp + translateHTMLReserved(s[i]);
			   }
			}
			break;
		
			case readesc:                                        //Melvin worked on case readesc
		    temp += translateHTMLReserved(s[i]);
			i++;
			if(s[i]== 'n'){
			   temp += s[i];
			   newS += hlspans[hlescseq] + temp + spanend;
			   temp.clear();
			   state = strlit;
			   }
			else{
			   temp += s[i];
			   newS += hlspans[hlerror]+ temp + spanend;
			   temp.clear();
			   }
			   temp.clear();
			   break;
		
                case start:                                           //Yu Bin worked on case start 
					if(s[i]!='"')                                     //and case strlit
                        newS += translateHTMLReserved(s[i]);

					if(s[i]=='"'){
                        temp += translateHTMLReserved(s[i]);
                        newS+=hlspans[hlstrlit]+temp+spanend;
                }
                        break;


 
                case strlit:
                        temp += s[i];
                        newS += hlspans[hlstrlit] + temp + spanend;
                        break;


                case scannum:                                               //Lawrecen did case scannum, comment, error, readfs
                        newS += hlspans[hlnumeric] + s[i] + spanend;
                        break;

                case comment:
                        newS += hlspans[hlcomment] + s[i] + spanend;
                        break;

                case error:
                        temp += s[i];
                        newS += hlspans[hlerror] + temp + spanend;
                        break;
				case readfs:
                        state = comment;


        }

}

        return newS;
}

int main() {                                 

string s;
while(getline(cin, s)){
   cout << processOneLine(s) << endl;
}
return 0;

}

